﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using mmc.FernandoPrestes.UserService.Domains;
using mmc.FernandoPrestes.UserService.Infra.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mmc.FernandoPrestes.UserService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    public class UserController : ControllerBase
    {
        private readonly IUserRepository repository;
        private readonly ILogger<UserController> _logger;

        public UserController(ILogger<UserController> logger, IUserRepository repository)
        {
            // Set Logger and UserRepository properties from Dependecy Injection instances.
            _logger = logger;
            this.repository = repository;
        }

        [HttpPost]
        public async Task<ServiceResponse<object>> Post(User user)
        {
            try
            {
                _logger.LogInformation("Insert user - validating insertion");
                var allowInsertion = await repository.ValideteInsertOneAsync(user.Email);

                if (!allowInsertion)
                {
                    _logger.LogInformation($"Insert user - validation fail");
                    return ServiceResponse<object>.Failure("User e-mail already inserted");
                }

                _logger.LogInformation("Inserting user");
                var document = new Infra.Repository.Entity.User
                {
                    Name = user.Name,
                    Email = user.Email,
                    Password = Guid.NewGuid().ToString().Substring(0, 7),
                    LastAccess = DateTime.Now
                };

                await repository.InsertOneAsync(document);
                return ServiceResponse<object>.Success("User inserted");
            }
            catch (Exception ex)
            {
                _logger.LogError("Error inserting user - {message}", ex.Message);
                return ServiceResponse<object>.Failure("Unexpected error");
            }
        }

        [HttpPut("{id}")]
        public async Task<ServiceResponse<object>> Put(string id, User user)
        {
            if (!id.Equals(user.Id))
            {
                _logger.LogInformation("Update user - validation fail");
                return ServiceResponse<object>.Failure("Invalid id");
            }

            try
            {
                _logger.LogInformation("Updating user");
                var mongoUser = await repository.FindAsync(id);

                if (mongoUser == null)
                {
                    return ServiceResponse<object>.Failure("User not found");
                }

                if (mongoUser.Email != user.Email)
                {
                    var allowInsertion = await repository.ValideteInsertOneAsync(user.Email);

                    if (!allowInsertion)
                    {
                        _logger.LogInformation($"Update user - validation fail");
                        return ServiceResponse<object>.Failure("E-mail already inserted");
                    }
                }

                var document = new Infra.Repository.Entity.User
                {
                    Id = user.Id,
                    Name = user.Name,
                    Email = user.Email,
                    Password = mongoUser.Password,
                    LastAccess = mongoUser.LastAccess
                };

                await repository.ReplaceOneAsync(document);
                    return ServiceResponse<object>.Success("User updated");
            }
            catch (Exception ex)
            {
                _logger.LogError("Error updating user - {message}", ex.Message);
                return ServiceResponse<object>.Failure("Unexpected error");
            }
        }

        [HttpGet]
        public async Task<ServiceResponse<List<User>>> List()
        {
            try
            {
                _logger.LogInformation("Retrieving users");
                var users = await repository.ListAsync();

                return ServiceResponse<List<User>>.Success(
                           "Users list", 
                           users.Select(u => new User
                                    {
                                        Id = u.Id,
                                        Name = u.Name,
                                        Email = u.Email,
                                        LastAccess = u.LastAccess
                                    })
                                .ToList()
                       );
            }
            catch (Exception ex)
            {
                _logger.LogError("Error retrieving users - {message}", ex.Message);
                return ServiceResponse<List<User>>.Failure("Unexpected error");
            }
        }

        [HttpGet("{id}")]
        public async Task<ServiceResponse<User>> Get(string id)
        {
            try
            {
                _logger.LogInformation("Retrieving users");
                var user = await repository.FindAsync(id);

                return ServiceResponse<User>.Success(
                           "User item",
                           new User
                           {
                               Id = user.Id,
                               Name = user.Name,
                               Email = user.Email,
                               LastAccess = user.LastAccess
                            }
                       );
            }
            catch (Exception ex)
            {
                _logger.LogError("Error retrieving users - {message}", ex.Message);
                return ServiceResponse<User>.Failure("Unexpected error");
            }
        }

        [HttpDelete]
        public async Task<ServiceResponse<object>> Delete(string id)
        {
            try
            {
                _logger.LogInformation("Deleting user");
                await repository.DeleteOneAsync(id);
                return ServiceResponse<object>.Success("User deleted");
            }
            catch (Exception ex)
            {
                _logger.LogError("Error deleting user - {message}", ex.Message);
                return ServiceResponse<object>.Failure("Unexpected error");
            }
        }
    }
}
