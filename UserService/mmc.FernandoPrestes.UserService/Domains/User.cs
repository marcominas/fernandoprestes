﻿using System;

namespace mmc.FernandoPrestes.UserService.Domains
{
    public class User
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public DateTime LastAccess { get; set; }
    }
}
