﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace mmc.FernandoPrestes.UserService.Config
{
    public static class ConfigureSettings
    {
        public static void AddSettings<T>(this IServiceCollection services, IConfiguration Configuration) where T : class
        {
            services.Configure<T>(Configuration.GetSection(typeof(T).Name));
        }
    }
}
