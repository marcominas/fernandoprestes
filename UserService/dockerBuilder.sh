#!/usr/bin/bash

if [[ -z $(ps -ef | grep docker | grep -v grep) ]]; then
    sudo service docker start
fi

if [ "$1" == "build" ]; then
    echo "building userService"
    docker build --tag mmc-fernando-prestes-user-service:1.0 .
fi

if [ "$1" == "run" ]; then
    echo "starting userService"
    docker rm --force user_service > /dev/null 2>&1
    docker run -dp 5011:80 --name user_service mmc-fernando-prestes-user-service:1.0 
fi

if [ "$1" == "stop" ]; then
    echo "stopping userService"
    docker stop user_service
fi
