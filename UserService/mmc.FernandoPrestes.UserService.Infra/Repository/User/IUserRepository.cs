﻿using mmc.FernandoPrestes.UserService.Infra.Repository.Entity;
using System.Threading.Tasks;

namespace mmc.FernandoPrestes.UserService.Infra.Repository
{
    public interface IUserRepository : IBaseRepository<User>
    {
        Task<bool> ValideteInsertOneAsync(string email);
    }
}
