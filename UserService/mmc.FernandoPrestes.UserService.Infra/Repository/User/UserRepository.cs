﻿using mmc.FernandoPrestes.UserService.Infra.Repository.Entity;
using MongoDB.Driver;
using System.Linq;
using System.Threading.Tasks;

namespace mmc.FernandoPrestes.UserService.Infra.Repository
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        public UserRepository(IMongoContext context) : base(context)
        {
            var indexKeysDefinition = Builders<User>.IndexKeys.Ascending(u => u.Email);
            _mongoCollection.Indexes.CreateOneAsync(new CreateIndexModel<User>(indexKeysDefinition));
        }

        public async Task<bool> ValideteInsertOneAsync(string email)
        {
            // var duplicated = await FindAsync("email", email);
            // var filter = Builders<User>.Filter.Eq("email", email);
            // var all = await _mongoCollection.FindAsync(filter);
            var allDocuments = await ListAsync();

            return !allDocuments.Any(u => u.Email == email);
        }
    }
}
