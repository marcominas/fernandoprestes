﻿using MongoDB.Driver;

namespace mmc.FernandoPrestes.UserService.Infra.Repository
{
    public interface IMongoContext
    {
        IMongoCollection<Book> GetCollection<Book>(string name);
    }
}