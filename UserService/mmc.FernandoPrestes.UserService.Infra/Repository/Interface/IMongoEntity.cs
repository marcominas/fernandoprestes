﻿namespace mmc.FernandoPrestes.UserService.Infra.Repository.Entity
{
    public interface IMongoEntity
    {
        string Id { get; set; }
    }
}
