﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace mmc.FernandoPrestes.UserService.Infra.Repository
{
    public interface IBaseRepository<TEntity> where TEntity : class
    {
        Task InsertOneAsync(TEntity document);
        Task ReplaceOneAsync(TEntity document);
        Task DeleteOneAsync(string id);
        Task<TEntity> FindAsync(string id);
        Task<IEnumerable<TEntity>> ListAsync();
        Task<IEnumerable<TEntity>> FindAsync(string fieldName, object value);
    }
}
