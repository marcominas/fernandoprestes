﻿using mmc.FernandoPrestes.UserService.Infra.Repository.Entity;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace mmc.FernandoPrestes.UserService.Infra.Repository
{
    public abstract class BaseRepository<T> : IBaseRepository<T> where T : class
    {
        protected readonly IMongoContext _mongoContext;
        protected readonly IMongoCollection<T> _mongoCollection;

        protected BaseRepository(IMongoContext context)
        {
            _mongoContext = context;
            _mongoCollection = _mongoContext.GetCollection<T>(typeof(T).Name);
        }

        public async Task InsertOneAsync(T document)
        {
            if (document == null)
            {
                throw new ArgumentNullException(typeof(T).Name + " object is null");
            }
            await _mongoCollection.InsertOneAsync(document);
        }

        public async Task DeleteOneAsync(string id)
        {
            var objectId = new ObjectId(id);
            await _mongoCollection.DeleteOneAsync(Builders<T>.Filter.Eq("_id", objectId));
        }

        public virtual async Task ReplaceOneAsync(T document)
        {
            var objectId = new ObjectId(document.GetId());
            await _mongoCollection.ReplaceOneAsync(Builders<T>.Filter.Eq("_id", objectId), document);
        }

        public async Task<T> FindAsync(string id)
        {
            var objectId = new ObjectId(id);
            var filter = Builders<T>.Filter.Eq("_id", objectId);
            return await _mongoCollection.FindAsync(filter).Result.FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<T>> ListAsync()
        {
            var all = await _mongoCollection.FindAsync(Builders<T>.Filter.Empty);
            return await all.ToListAsync();
        }

        public async Task<IEnumerable<T>> FindAsync(string fieldName, object value)
        {
            var filter = Builders<T>.Filter.Eq(fieldName, value);
            var filtered = await _mongoCollection.FindAsync(filter);
            return await filtered.ToListAsync();
        }
    }
}