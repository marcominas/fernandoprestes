﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace mmc.FernandoPrestes.UserService.Infra.Repository
{
    public class MongoContext : IMongoContext
    {
        private readonly IMongoDatabase _mongoDatabase;
        private readonly MongoClient _mongoClient;

        public MongoContext(IOptions<MongoSettings> configuration)
        {
            _mongoClient = new MongoClient(configuration.Value.Connection);
            _mongoDatabase = _mongoClient.GetDatabase(configuration.Value.DatabaseName);
        }

        public IMongoCollection<T> GetCollection<T>(string name)
        {
            return _mongoDatabase.GetCollection<T>(name);
        }
    }
}
