﻿namespace mmc.FernandoPrestes.UserService.Infra.Repository.Entity
{
    static public class EntityExtensions
    {
        public static string GetId(this object entity)
        {
            return ((IMongoEntity)entity).Id;
        }
    }
}