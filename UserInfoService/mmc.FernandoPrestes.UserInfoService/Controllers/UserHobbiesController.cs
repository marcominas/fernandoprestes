﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using mmc.FernandoPrestes.UserInfoService.Domains;
using mmc.FernandoPrestes.UserInfoService.Infra.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mmc.FernandoPrestes.UserInfoService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    public class UserHobbiesController : ControllerBase
    {
        private readonly IUserHobbiesRepository repository;
        private readonly ILogger<UserHobbiesController> _logger;

        public UserHobbiesController(ILogger<UserHobbiesController> logger, IUserHobbiesRepository repository)
        {
            // Set Logger and UserHobbiesRepository properties from Dependecy Injection instances.
            _logger = logger;
            this.repository = repository;
        }

        [HttpPost]
        public async Task<ActionResult> Post(UserHobbies userHobbies)
        {
            try
            {
                _logger.LogInformation("Insert userHobbies - validating insertion");

                _logger.LogInformation("Inserting userHobbies");
                var document = new Infra.Repository.Entity.UserHobbies
                {
                    UserId = userHobbies.UserId,
                    Hobbies = userHobbies.Hobbies
                };

                await repository.InsertOneAsync(document);
                return Ok("UserHobbies inserted");
            }
            catch (Exception ex)
            {
                _logger.LogError("Error inserting userHobbies - {message}", ex.Message);
                return Problem("Unexpected error");
            }
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Put(string id, UserHobbies userHobbies)
        {
            if (!id.Equals(userHobbies.Id))
            {
                _logger.LogInformation("Update userHobbies - validation fail");
                return ValidationProblem("Invalid id");
            }

            try
            {
                _logger.LogInformation("Updating userHobbies");
                var document = new Infra.Repository.Entity.UserHobbies
                {
                    Id = userHobbies.Id,
                    UserId = userHobbies.UserId,
                    Hobbies = userHobbies.Hobbies
                };

                await repository.ReplaceOneAsync(document);
                return Ok("UserHobbies updated");
            }
            catch (Exception ex)
            {
                _logger.LogError("Error updating userHobbies - {message}", ex.Message);
                return Problem("Unexpected error");
            }
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserHobbies>>> Get()
        {
            try
            {
                _logger.LogInformation("Retrieving userHobbies");
                var userHobbies = await repository.ListAsync();

                var userHobbiesResponse = userHobbies.Select(u => new UserHobbies
                {
                    UserId = u.UserId,
                    Hobbies = u.Hobbies
                });

                return Ok(userHobbiesResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error retrieving userHobbies - {message}", ex.Message);
                return Problem("Unexpected error");
            }
        }

        [HttpDelete]
        public async Task<ActionResult> Delete(string id)
        {
            try
            {
                _logger.LogInformation("Deleting userHobbies");
                await repository.DeleteOneAsync(id);
                return Ok("UserHobbies deleted");
            }
            catch (Exception ex)
            {
                _logger.LogError("Error deleting userHobbies - {message}", ex.Message);
                return Problem("Unexpected error");
            }
        }
    }
}
