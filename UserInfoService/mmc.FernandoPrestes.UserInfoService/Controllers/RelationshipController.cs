﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using mmc.FernandoPrestes.UserInfoService.Domains;
using mmc.FernandoPrestes.UserInfoService.Infra.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mmc.FernandoPrestes.UserInfoService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    public class RelationshipController : ControllerBase
    {
        private readonly IRelationshipRepository repository;
        private readonly ILogger<RelationshipController> _logger;

        public RelationshipController(ILogger<RelationshipController> logger, IRelationshipRepository repository)
        {
            // Set Logger and RelationshipRepository properties from Dependecy Injection instances.
            _logger = logger;
            this.repository = repository;
        }

        [HttpPost]
        public async Task<ActionResult> Post(Relationship relationship)
        {
            try
            {
                _logger.LogInformation("Insert relationship - validating insertion");
                var allowInsertion = await repository.ValideteInsertOneAsync(relationship.Name);

                if (!allowInsertion)
                {
                    _logger.LogInformation($"Insert relationship - validation fail");
                    return ValidationProblem("Relationship name already inserted");
                }

                _logger.LogInformation("Inserting relationship");
                var document = new Infra.Repository.Entity.Relationship
                {
                    Name = relationship.Name,
                    Description = relationship.Description,
                    Type = relationship.Type
                };

                await repository.InsertOneAsync(document);
                return Ok("Relationship inserted");
            }
            catch (Exception ex)
            {
                _logger.LogError("Error inserting relationship - {message}", ex.Message);
                return Problem("Unexpected error");
            }
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Put(string id, Relationship relationship)
        {
            if (!id.Equals(relationship.Id))
            {
                _logger.LogInformation("Update relationship - validation fail");
                return ValidationProblem("Invalid id");
            }

            try
            {
                _logger.LogInformation("Updating relationship");
                var document = new Infra.Repository.Entity.Relationship
                {
                    Id = relationship.Id,
                    Name = relationship.Name,
                    Description = relationship.Description,
                    Type = relationship.Type
                };

                await repository.ReplaceOneAsync(document);
                return Ok("Relationship updated");
            }
            catch (Exception ex)
            {
                _logger.LogError("Error updating relationship - {message}", ex.Message);
                return Problem("Unexpected error");
            }
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Relationship>>> Get()
        {
            try
            {
                _logger.LogInformation("Retrieving relationships");
                var relationships = await repository.ListAsync();

                var relationshipResponse = relationships.Select(u => new Relationship
                {
                    Id = u.Id,
                    Name = u.Name, 
                    Type = u.Type,
                    Description = u.Description
                });

                return Ok(relationshipResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error retrieving relationships - {message}", ex.Message);
                return Problem("Unexpected error");
            }
        }

        [HttpDelete]
        public async Task<ActionResult> Delete(string id)
        {
            try
            {
                _logger.LogInformation("Deleting relationship");
                await repository.DeleteOneAsync(id);
                return Ok("Relationship deleted");
            }
            catch (Exception ex)
            {
                _logger.LogError("Error deleting relationship - {message}", ex.Message);
                return Problem("Unexpected error");
            }
        }
    }
}
