﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using mmc.FernandoPrestes.UserInfoService.Domains;
using mmc.FernandoPrestes.UserInfoService.Infra.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mmc.FernandoPrestes.UserInfoService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    public class HobbyController : ControllerBase
    {
        private readonly IHobbyRepository _repository;
        private readonly ILogger<HobbyController> _logger;

        public HobbyController(ILogger<HobbyController> logger, IHobbyRepository repository)
        {
            // Set Logger and HobbyRepository properties from Dependecy Injection instances.
            _logger = logger;
            _repository = repository;
        }

        [HttpPost]
        public async Task<ActionResult> Post(Hobby hobby)
        {
            try
            {
                _logger.LogInformation("Insert hobby - validating insertion");
                var allowInsertion = await _repository.ValideteInsertOneAsync(hobby.Name);

                if (!allowInsertion)
                {
                    _logger.LogInformation($"Insert hobby - validation fail");
                    return ValidationProblem("hobby name already inserted");
                }

                _logger.LogInformation("Inserting hobby");
                var document = new Infra.Repository.Entity.Hobby
                {
                    Name = hobby.Name,
                    Description = hobby.Description
                };

                await _repository.InsertOneAsync(document);
                return Ok("Hobby inserted");
            }
            catch (Exception ex)
            {
                _logger.LogError("Error inserting hobby - {message}", ex.Message);
                return Problem("Unexpected error");
            }
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Put(string id, Hobby hobby)
        {
            if (!id.Equals(hobby.Id))
            {
                _logger.LogInformation("Update hobby - validation fail");
                return ValidationProblem("Invalid id");
            }

            try
            {
                _logger.LogInformation("Updating hobby");
                var document = new Infra.Repository.Entity.Hobby
                {
                    Id = hobby.Id,
                    Name = hobby.Name,
                    Description = hobby.Description,
                };

                await _repository.ReplaceOneAsync(document);
                return Ok("Hobby updated");
            }
            catch (Exception ex)
            {
                _logger.LogError("Error updating hobby - {message}", ex.Message);
                return Problem("Unexpected error");
            }
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Hobby>>> Get()
        {
            try
            {
                _logger.LogInformation("Retrieving hobbies");
                var hobbies = await _repository.ListAsync();

                var hobbyResponse = hobbies.Select(h => new Hobby
                {
                    Id = h.Id,
                    Name = h.Name,
                    Description = h.Description
                });

                return Ok(hobbyResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error retrieving hobbies - {message}", ex.Message);
                return Problem("Unexpected error");
            }
        }

        [HttpDelete]
        public async Task<ActionResult> Delete(string id)
        {
            try
            {
                _logger.LogInformation("Deleting hobby");
                await _repository.DeleteOneAsync(id);
                return Ok("Hobby deleted");
            }
            catch (Exception ex)
            {
                _logger.LogError("Error deleting hobby - {message}", ex.Message);
                return Problem("Unexpected error");
            }
        }
    }
}
