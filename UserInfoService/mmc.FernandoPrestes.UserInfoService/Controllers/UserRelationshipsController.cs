﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using mmc.FernandoPrestes.UserInfoService.Domains;
using mmc.FernandoPrestes.UserInfoService.Infra.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mmc.FernandoPrestes.UserInfoService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    public class UserRelationshipsController : ControllerBase
    {
        private readonly IUserRelationshipsRepository repository;
        private readonly ILogger<UserRelationshipsController> _logger;

        public UserRelationshipsController(ILogger<UserRelationshipsController> logger, IUserRelationshipsRepository repository)
        {
            // Set Logger and UserRelationshipsRepository properties from Dependecy Injection instances.
            _logger = logger;
            this.repository = repository;
        }

        [HttpPost]
        public async Task<ActionResult> Post(UserRelationships userRelationships)
        {
            try
            {
                _logger.LogInformation("Insert userRelationships - validating insertion");

                _logger.LogInformation("Inserting userRelationships");
                var document = new Infra.Repository.Entity.UserRelationships
                {
                    UserId = userRelationships.UserId,
                    Relationships = userRelationships.Relationships.Select(h =>
                                                    new Infra.Repository.Entity.UserRelationship
                                                    {
                                                        Name = h.Name, 
                                                        Type = h.Type
                                                    })
                                                 .ToList()
                };

                await repository.InsertOneAsync(document);
                return Ok("UserRelationships inserted");
            }
            catch (Exception ex)
            {
                _logger.LogError("Error inserting userRelationships - {message}", ex.Message);
                return Problem("Unexpected error");
            }
        }

        [HttpPut("{userId}")]
        public async Task<ActionResult> Put(string userId, UserRelationships userRelationships)
        {
            if (!userId.Equals(userRelationships.UserId))
            {
                _logger.LogInformation("Update userRelationships - validation fail");
                return ValidationProblem("Invalid id");
            }

            try
            {
                _logger.LogInformation("Updating userRelationships");
                var document = new Infra.Repository.Entity.UserRelationships
                {
                    UserId = userRelationships.UserId,
                    Relationships = userRelationships.Relationships.Select(h =>
                                                    new Infra.Repository.Entity.UserRelationship
                                                    {
                                                        Name = h.Name,
                                                        Type = h.Type
                                                    })
                                                 .ToList()
                };

                await repository.ReplaceOneAsync(document);
                return Ok("UserRelationships updated");
            }
            catch (Exception ex)
            {
                _logger.LogError("Error updating userRelationships - {message}", ex.Message);
                return Problem("Unexpected error");
            }
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserRelationships>>> Get()
        {
            try
            {
                _logger.LogInformation("Retrieving userRelationships");
                var userRelationships = await repository.ListAsync();

                var userRelationshipsResponse = userRelationships.Select(u => new UserRelationships
                {
                    UserId = u.UserId,
                    Relationships = u.Relationships.Select(h =>
                                                        new UserRelationship
                                                        {
                                                            Name = h.Name,
                                                            Type = h.Type
                                                        })
                                                   .ToList()
                });

                return Ok(userRelationshipsResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error retrieving userRelationships - {message}", ex.Message);
                return Problem("Unexpected error");
            }
        }

        [HttpDelete]
        public async Task<ActionResult> Delete(string id)
        {
            try
            {
                _logger.LogInformation("Deleting userRelationships");
                await repository.DeleteOneAsync(id);
                return Ok("UserRelationships deleted");
            }
            catch (Exception ex)
            {
                _logger.LogError("Error deleting userRelationships - {message}", ex.Message);
                return Problem("Unexpected error");
            }
        }
    }
}
