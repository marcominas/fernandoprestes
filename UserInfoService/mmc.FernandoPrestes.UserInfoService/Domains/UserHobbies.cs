﻿using System.Collections.Generic;

namespace mmc.FernandoPrestes.UserInfoService.Domains
{
    public class UserHobbies : UserInfo
    {
        public IList<string> Hobbies { get; set; }
    }
}
