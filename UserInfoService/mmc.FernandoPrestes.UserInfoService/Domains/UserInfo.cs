﻿namespace mmc.FernandoPrestes.UserInfoService.Domains
{
    public class UserInfo
    {
        public string Id { get; set; }
        public string UserId { get; set; }
    }
}
