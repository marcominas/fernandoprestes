﻿using mmc.FernandoPrestes.UserInfoService.Infra.Repository.Entity;

namespace mmc.FernandoPrestes.UserInfoService.Domains
{
    public class Relationship
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public RelationshipType Type { get; set; }
        public string Description { get; set; }
    }
}
