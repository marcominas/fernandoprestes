﻿
namespace mmc.FernandoPrestes.UserInfoService.Domains
{
    public class Hobby
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
