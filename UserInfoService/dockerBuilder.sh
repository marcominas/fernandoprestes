#!/usr/bin/bash

if [[ -z $(ps -ef | grep docker | grep -v grep) ]]; then
    sudo service docker start
fi

if [ "$1" == "build" ]; then
    echo "building userInfoService"
    docker build --tag mmc-fernando-prestes-user-info-service:1.0 .
fi

if [ "$1" == "run" ]; then
    echo "starting userInfoService"
    docker rm --force user_info_service > /dev/null 2>&1
    docker run -dp 5012:80 --name user_info_service mmc-fernando-prestes-user-info-service:1.0 
fi

if [ "$1" == "stop" ]; then
    echo "stopping userInfoService"
    docker stop user_info_service
fi
