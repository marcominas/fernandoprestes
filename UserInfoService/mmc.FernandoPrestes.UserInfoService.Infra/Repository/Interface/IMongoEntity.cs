﻿namespace mmc.FernandoPrestes.UserInfoService.Infra.Repository.Entity
{
    public interface IMongoEntity
    {
        string Id { get; set; }
    }
}
