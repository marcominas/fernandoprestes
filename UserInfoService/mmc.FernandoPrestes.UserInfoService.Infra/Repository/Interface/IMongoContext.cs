﻿using MongoDB.Driver;

namespace mmc.FernandoPrestes.UserInfoService.Infra.Repository
{
    public interface IMongoContext
    {
        IMongoCollection<Book> GetCollection<Book>(string name);
    }
}