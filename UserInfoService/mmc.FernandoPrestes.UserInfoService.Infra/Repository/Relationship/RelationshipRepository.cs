﻿using mmc.FernandoPrestes.UserInfoService.Infra.Repository.Entity;
using MongoDB.Driver;
using System.Linq;
using System.Threading.Tasks;

namespace mmc.FernandoPrestes.UserInfoService.Infra.Repository
{
    public class RelationshipRepository : BaseRepository<Relationship>, IRelationshipRepository
    {
        public RelationshipRepository(IMongoContext context) : base(context)
        {
            var indexKeysDefinition = Builders<Relationship>.IndexKeys.Ascending(h => h.Name);
            _mongoCollection.Indexes.CreateOneAsync(new CreateIndexModel<Relationship>(indexKeysDefinition));
        }

        public async Task<bool> ValideteInsertOneAsync(string name)
        {
            var allDocuments = await ListAsync();

            return !allDocuments.Any(u => u.Name == name);
        }
    }
}
