﻿using mmc.FernandoPrestes.UserInfoService.Infra.Repository.Entity;
using System.Threading.Tasks;

namespace mmc.FernandoPrestes.UserInfoService.Infra.Repository
{
    public interface IRelationshipRepository : IBaseRepository<Relationship>
    {
        Task<bool> ValideteInsertOneAsync(string name);
    }
}
