﻿using mmc.FernandoPrestes.UserInfoService.Infra.Repository.Entity;
using MongoDB.Driver;

namespace mmc.FernandoPrestes.UserInfoService.Infra.Repository
{
    public class UserRelationshipsRepository : BaseRepository<Entity.UserRelationships>, IUserRelationshipsRepository
    {
        public UserRelationshipsRepository(IMongoContext context) : base(context)
        {
            var indexKeysDefinition = Builders<Entity.UserRelationships>.IndexKeys.Ascending(h => h.UserId);
            _mongoCollection.Indexes.CreateOneAsync(new CreateIndexModel<Entity.UserRelationships>(indexKeysDefinition));
        }
    }
}
