﻿using mmc.FernandoPrestes.UserInfoService.Infra.Repository.Entity;

namespace mmc.FernandoPrestes.UserInfoService.Infra.Repository
{
    public interface IUserRelationshipsRepository : IBaseRepository<UserRelationships>
    {
    }
}
