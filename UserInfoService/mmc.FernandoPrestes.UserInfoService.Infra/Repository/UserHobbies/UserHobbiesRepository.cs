﻿using mmc.FernandoPrestes.UserInfoService.Infra.Repository.Entity;
using MongoDB.Driver;

namespace mmc.FernandoPrestes.UserInfoService.Infra.Repository
{
    public class UserHobbiesRepository : BaseRepository<UserHobbies>, IUserHobbiesRepository
    {
        public UserHobbiesRepository(IMongoContext context) : base(context)
        {
            var indexKeysDefinition = Builders<UserHobbies>.IndexKeys.Ascending(h => h.UserId);
            _mongoCollection.Indexes.CreateOneAsync(new CreateIndexModel<UserHobbies>(indexKeysDefinition));
        }
    }
}
