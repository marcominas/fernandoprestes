﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace mmc.FernandoPrestes.UserInfoService.Infra.Repository.Entity
{
    public class Hobby : IMongoEntity
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
