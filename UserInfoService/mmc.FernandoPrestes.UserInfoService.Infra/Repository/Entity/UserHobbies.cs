﻿using System.Collections.Generic;

namespace mmc.FernandoPrestes.UserInfoService.Infra.Repository.Entity
{
    public class UserHobbies : UserInfo
    {
        public IList<string> Hobbies { get; set; }
    }
}
