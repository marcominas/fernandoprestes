﻿namespace mmc.FernandoPrestes.UserInfoService.Infra.Repository.Entity
{
    public enum RelationshipType : byte
    {
        Family = 1,
        Social = 2,
        Professional = 3
    }
}
