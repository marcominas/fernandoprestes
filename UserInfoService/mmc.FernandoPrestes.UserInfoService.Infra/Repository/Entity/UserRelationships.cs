﻿using System.Collections.Generic;

namespace mmc.FernandoPrestes.UserInfoService.Infra.Repository.Entity
{
    public class UserRelationship
    {
        public string Name { get; set; }
        public string Type { get; set; }
    }

    public class UserRelationships : UserInfo
    {
        public IList<UserRelationship> Relationships { get; set; }
    }
}
