﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace mmc.FernandoPrestes.UserInfoService.Infra.Repository.Entity
{
    public class UserInfo : IMongoEntity
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string UserId { get; set; }
    }
}
