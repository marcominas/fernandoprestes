﻿using mmc.FernandoPrestes.UserInfoService.Infra.Repository.Entity;
using MongoDB.Driver;
using System.Linq;
using System.Threading.Tasks;

namespace mmc.FernandoPrestes.UserInfoService.Infra.Repository
{
    public class HobbyRepository : BaseRepository<Hobby>, IHobbyRepository
    {
        public HobbyRepository(IMongoContext context) : base(context)
        {
            var indexKeysDefinition = Builders<Hobby>.IndexKeys.Ascending(h => h.Name);
            _mongoCollection.Indexes.CreateOneAsync(new CreateIndexModel<Hobby>(indexKeysDefinition));
        }

        public async Task<bool> ValideteInsertOneAsync(string name)
        {
            var allDocuments = await ListAsync();

            return !allDocuments.Any(u => u.Name == name);
        }
    }
}
