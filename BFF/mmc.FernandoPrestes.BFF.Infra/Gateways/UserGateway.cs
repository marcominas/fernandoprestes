﻿using mmc.FernandoPrestes.BFF.Core.Domains;
using mmc.FernandoPrestes.BFF.Infra.ServiceGateways;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace mmc.FernandoPrestes.BFF.Infra.Gateways
{
    public class UserGateway : IUserGateway
    {
        private readonly IUserServiceGateway _serviceGateway;

        public UserGateway(IUserServiceGateway serviceGateway)
        {
            _serviceGateway = serviceGateway;
        }

        public async Task<ServiceResponse<List<User>>> ListAsync()
        {
            try
            {
                var response = await _serviceGateway.ListUsers();
                return response;
            }
            catch (Exception ex)
            {
                return ServiceResponse<List<User>>.Failure(
                           "Unable to get users", 
                           new List<object> { ex }
                       );
            }
        }

        public async Task<ServiceResponse<User>> GetAsync(string id)
        {
            try
            {
                var response = await _serviceGateway.GetUser(id);
                return response;
            }
            catch (Exception ex)
            {
                return ServiceResponse<User>.Failure(
                           "Unable to get users", 
                           new List<object> { ex }
                       );
            }
        }

        public async Task<ServiceResponse<object>> InsertAsync(User user)
        {
            try
            {
                var result = await _serviceGateway.InsertUser(user);
                return result;
            }
            catch (Exception ex)
            {
                var errors = new List<object> { ex.Message };
                if (ex.InnerException != null)
                {
                    errors.Add(ex.InnerException.Message);
                }
                return ServiceResponse<object>.Failure("Unable to get users", errors);
            }
        }

        public async Task<ServiceResponse<object>> UpdateAsync(string id, User entity)
        {
            try
            {
                var response = await _serviceGateway.UpdateUser(id, entity);
                return response;
            }
            catch (Exception ex)
            {
                return ServiceResponse<object>.Failure(
                           "Unable to get users", 
                           new List<object> { ex }
                       );
            }
        }
    }
}
