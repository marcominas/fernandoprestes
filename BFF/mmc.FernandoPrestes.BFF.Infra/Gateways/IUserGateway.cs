﻿using mmc.FernandoPrestes.BFF.Core.Domains;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace mmc.FernandoPrestes.BFF.Infra.Gateways
{
    public interface IUserGateway
    {
        Task<ServiceResponse<List<User>>> ListAsync();
        Task<ServiceResponse<User>> GetAsync(string id);
        Task<ServiceResponse<object>> InsertAsync(User user);
    }
}
