﻿using mmc.FernandoPrestes.BFF.Core.Domains;
using mmc.FernandoPrestes.BFF.Core.Repositories;
using mmc.FernandoPrestes.BFF.Infra.Gateways;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace mmc.FernandoPrestes.BFF.Infra.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly IUserGateway _userGateway;

        public UserRepository(IUserGateway userGateway)
        {
            _userGateway = userGateway;
        }

        public async Task<ServiceResponse<object>> AddAsync(User entity)
        {
            if (entity == null)
            {
                return ServiceResponse<object>.Failure("Invalid request");
            }

            var result = await _userGateway.InsertAsync(entity);

            return result;
        }

        public async Task<ServiceResponse<User>> GetAsync(string id)
        {
            var result = await _userGateway.GetAsync(id);

            return result;
        }

        public  async Task<ServiceResponse<List<User>>> ListAsync()
        {
            var result = await _userGateway.ListAsync();

            return result;
        }
    }
}
