﻿using mmc.FernandoPrestes.BFF.Core.Domains;
using Refit;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace mmc.FernandoPrestes.BFF.Infra.ServiceGateways
{
    public interface IUserServiceGateway
    {
        [Get("/user/{id}")]
        Task<ServiceResponse<User>> GetUser(string id);

        [Get("/user")]
        Task<ServiceResponse<List<User>>> ListUsers();

        [Post("/user")]
        Task<ServiceResponse<object>> InsertUser(User user);

        [Put("/user/{id}")]
        Task<ServiceResponse<object>> UpdateUser(string id, User user);
    }
}
