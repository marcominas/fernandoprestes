﻿using MediatR;
using mmc.FernandoPrestes.BFF.Core.Domains;
using mmc.FernandoPrestes.BFF.Core.Repositories;
using System.Threading;
using System.Threading.Tasks;

namespace mmc.FernandoPrestes.BFF.Core.Commands
{
    public class CreateUserCommandHandler : IRequestHandler<CreateUserCommand, ServiceResponse<object>>
    {
        private readonly IUserRepository _repository;

        public CreateUserCommandHandler(IUserRepository repository)
        {
            _repository = repository;
        }

        public async Task<ServiceResponse<object>> Handle(CreateUserCommand request, CancellationToken cancellationToken)
        {
            var result = await _repository.AddAsync(request.User);
            return result;
        }
    }
}
