﻿using MediatR;
using mmc.FernandoPrestes.BFF.Core.Domains;

namespace mmc.FernandoPrestes.BFF.Core.Commands
{
    public class CreateUserCommand : IRequest<ServiceResponse<object>>
    {
        public User User { get; set; }
    }
}
