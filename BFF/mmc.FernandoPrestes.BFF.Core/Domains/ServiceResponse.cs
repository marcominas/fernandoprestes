﻿using System.Collections.Generic;

namespace mmc.FernandoPrestes.BFF.Core.Domains
{
    public enum ServiceResponseStatus : byte
    {
        Success,
        Failure
    }

    public class ServiceResponse<TEntity> where TEntity : class, new()
    {
        public bool IsSuccess
        {
            get
            {
                return Status == ServiceResponseStatus.Success;
            }
        }

        private ServiceResponseStatus Status { get; set; }
        public string Message { get; set; }
        public IList<object> Errors { get; set; }
        public TEntity Data { get; set; }

        public static ServiceResponse<TEntity> Success(string message, TEntity value = null)
        {
            return new ServiceResponse<TEntity>
            {
                Message = message,
                Status = ServiceResponseStatus.Success,
                Data = value
            };
        }
        public static ServiceResponse<TEntity> Failure(string message, IList<object> errors = null)
        {
            return new ServiceResponse<TEntity>
            {
                Message = message,
                Status = ServiceResponseStatus.Failure,
                Errors = errors
            };
        }
    }
}
