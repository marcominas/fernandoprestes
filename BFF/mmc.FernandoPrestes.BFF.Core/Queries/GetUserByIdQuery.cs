﻿using MediatR;
using mmc.FernandoPrestes.BFF.Core.Domains;

namespace mmc.FernandoPrestes.BFF.Core.Queries
{
    public class GetUserByIdQuery : IRequest<ServiceResponse<User>>
    {
        public string Id { get; set; }
    }
}
