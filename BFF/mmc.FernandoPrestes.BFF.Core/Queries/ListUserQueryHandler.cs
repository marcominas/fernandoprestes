﻿using MediatR;
using mmc.FernandoPrestes.BFF.Core.Domains;
using mmc.FernandoPrestes.BFF.Core.Repositories;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace mmc.FernandoPrestes.BFF.Core.Queries
{
    public class ListUserQueryHandler : IRequestHandler<ListUsersQuery, ServiceResponse<List<User>>>
    {
        private readonly IUserRepository _repository;

        public ListUserQueryHandler(IUserRepository repository)
        {
            _repository = repository;
        }

        public async Task<ServiceResponse<List<User>>> Handle(ListUsersQuery request, CancellationToken cancellationToken)
        {
            var result = await _repository.ListAsync();
            return result;
        }
    }
}
