﻿using MediatR;
using mmc.FernandoPrestes.BFF.Core.Domains;
using mmc.FernandoPrestes.BFF.Core.Repositories;
using System.Threading;
using System.Threading.Tasks;

namespace mmc.FernandoPrestes.BFF.Core.Queries
{
    public class GetUserByIdQueryHandler : IRequestHandler<GetUserByIdQuery, ServiceResponse<User>>
    {
        private readonly IUserRepository _repository;

        public GetUserByIdQueryHandler(IUserRepository repository)
        {
            _repository = repository;
        }

        public async Task<ServiceResponse<User>> Handle(GetUserByIdQuery request, CancellationToken cancellationToken)
        {
            var result = await _repository.GetAsync(request.Id);
            return result;
        }
    }
}
