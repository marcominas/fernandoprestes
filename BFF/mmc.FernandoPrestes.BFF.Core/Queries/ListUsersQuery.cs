﻿using MediatR;
using mmc.FernandoPrestes.BFF.Core.Domains;
using System.Collections.Generic;

namespace mmc.FernandoPrestes.BFF.Core.Queries
{
    public class ListUsersQuery : IRequest<ServiceResponse<List<User>>>
    {
    }
}
