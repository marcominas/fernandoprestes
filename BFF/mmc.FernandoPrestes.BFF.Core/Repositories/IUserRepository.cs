﻿using mmc.FernandoPrestes.BFF.Core.Domains;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace mmc.FernandoPrestes.BFF.Core.Repositories
{
    public interface IUserRepository
    {
        Task<ServiceResponse<User>> GetAsync(string id);

        Task<ServiceResponse<List<User>>> ListAsync();

        Task<ServiceResponse<object>> AddAsync(User entity);
    }
}
