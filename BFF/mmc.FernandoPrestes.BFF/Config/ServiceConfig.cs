﻿namespace mmc.FernandoPrestes.BFF.Config
{
    public class ServiceConfig
    {
        public string Name { get; set; }
        public string Url { get; set; }
    }
}
