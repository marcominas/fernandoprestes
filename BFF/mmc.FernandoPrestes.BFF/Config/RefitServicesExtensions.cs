﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Refit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;

namespace mmc.FernandoPrestes.BFF.Config
{
    public static class RefitServicesExtensions
    {
        public static IEnumerable<ServiceConfig> GetServicesConfig(this IConfiguration configuration, string sectionName)
        {
            var services = new List<ServiceConfig>();
            new ConfigureFromConfigurationOptions<List<ServiceConfig>>(
                    configuration.GetSection(sectionName))
                .Configure(services);

            return services;
        }

        public static void AddRefitClient<T>(this IEnumerable<ServiceConfig> refitConfigItems, IServiceCollection services, string serviceName) where T : class
        {
            var serviceConfig = refitConfigItems.FirstOrDefault(f => f.Name == serviceName)
                                ?? throw new SystemException($"Missing configuration for {serviceName}");

            var serializerSettings = new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase };
            var settings = new RefitSettings(new SystemTextJsonContentSerializer(serializerSettings));
            services.AddRefitClient<T>(settings)
                .ConfigureHttpClient(c => c.BaseAddress = new Uri(serviceConfig.Url));
        }
    }
}
