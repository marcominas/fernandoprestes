﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using mmc.FernandoPrestes.BFF.Services.RefitSample;
using mmc.FernandoPrestes.BFF.Services.RefitSample.Responses;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace mmc.FernandoPrestes.BFF.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AnimalController : ControllerBase
    {
        private readonly ILogger<AnimalController> _logger;

        public AnimalController(ILogger<AnimalController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public async Task<IEnumerable<SearchResult>> Get(
            [Required][FromQuery] AnimalType animalType,
            [Required] string breed_id)
        {
            _logger.LogInformation($"Getting {animalType}(s) of breed_id {breed_id}");
            var service = new AnimalService(GetUriString(animalType));
            var response = await service.Search(breed_id);

            _logger.LogInformation($"Got {response.Count()} item(s)");
            return response;
        }

        [HttpGet("dog")]
        public async Task<IEnumerable<SearchResult>> GetDogs([Required] string breed_id)
        {
            _logger.LogInformation($"Getting dog(s) of breed_id {breed_id}");
            var service = new AnimalService(GetUriString(AnimalType.Dog));
            var response = await service.Search(breed_id);

            _logger.LogInformation($"Got {response.Count()} item(s)");
            return response;
        }

        [HttpGet("cat")]
        public async Task<IEnumerable<SearchResult>> GetCats([Required] string breed_id)
        {
            _logger.LogInformation($"Getting cat(s) of breed_id {breed_id}");
            var service = new AnimalService(GetUriString(AnimalType.Cat));
            var response = await service.Search(breed_id);

            _logger.LogInformation($"Got {response.Count()} item(s)");
            return response;
        }

        [HttpGet("breed/filter")]
        public async Task<IEnumerable<BreedFilter>> GetBreedFilters([Required][FromQuery] AnimalType animalType)
        {
            _logger.LogInformation($"Getting breeds of {animalType}");
            var service = new AnimalService(GetUriString(animalType));
            var response = await service.GetBreedFilters();

            return response;
        }

        private Uri GetUriString(AnimalType animalType)
        {
            var animal = animalType.ToString().ToLower();
            return new Uri($"https://api.the{animal}api.com");
        }
    }
}