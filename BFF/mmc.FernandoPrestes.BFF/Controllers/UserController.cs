﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using mmc.FernandoPrestes.BFF.Core.Commands;
using mmc.FernandoPrestes.BFF.Core.Domains;
using mmc.FernandoPrestes.BFF.Core.Queries;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace mmc.FernandoPrestes.BFF.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    public class UserController : ControllerBase
    {
        private readonly ILogger<WeatherForecastController> _logger;
        private readonly IMediator _mediator;

        public UserController(ILogger<WeatherForecastController> logger, IMediator mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<ServiceResponse<List<User>>> List()
        {
            var query = new ListUsersQuery { };
            var result = await _mediator.Send(query);

            return result;
        }

        [HttpGet("{id}")]
        public async Task<ServiceResponse<User>> Get(string id)
        {
            var query = new GetUserByIdQuery { Id = id };
            var result = await _mediator.Send(query);

            return result;
        }

        [HttpPost]
        public async Task<ServiceResponse<object>> Post(User user)
        {
            var command = new CreateUserCommand { User = user };
            var result = await _mediator.Send(command);

            return result;
        }
    }
}
