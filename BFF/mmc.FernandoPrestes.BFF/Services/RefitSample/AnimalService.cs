﻿using mmc.FernandoPrestes.BFF.Services.Middleware;
using mmc.FernandoPrestes.BFF.Services.RefitSample.Responses;
using Refit;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace mmc.FernandoPrestes.BFF.Services.RefitSample
{
    public class AnimalService
    {
        private readonly HttpClient _httpClient;
        private readonly IAnimalAPI _animalApi;

        public AnimalService(Uri baseUrl)
        {
            var httpClientHandler = new HttpClientHandler();
            var httpClientDiagnosticsHandler = new HttpClientDiagnosticsHandler(httpClientHandler);

            _httpClient = new HttpClient(httpClientDiagnosticsHandler) { BaseAddress = baseUrl };
            _animalApi = RestService.For<IAnimalAPI>(_httpClient);
        }

        public async Task<IEnumerable<SearchResult>> Search(string breed_id)
        {
            return await _animalApi.Search(breed_id);
        }

        public async Task<IEnumerable<Breed>> GetBreeds()
        {
            return await _animalApi.GetBreeds();
        }

        public async Task<IEnumerable<BreedFilter>> GetBreedFilters()
        {
            return await _animalApi.GetBreedFilters();
        }
    }
}