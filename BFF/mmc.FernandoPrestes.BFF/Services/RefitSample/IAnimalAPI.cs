﻿using mmc.FernandoPrestes.BFF.Services.RefitSample.Responses;
using Refit;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace mmc.FernandoPrestes.BFF.Services.RefitSample
{
    [Headers("x-api-key: b95bfb30-55bc-4327-bb8b-35d740f70051")]
    public interface IAnimalAPI
    {
        [Get("/v1/images/search")]
        Task<IEnumerable<SearchResult>> Search([AliasAs("breed_id")] string breedIdentifier);

        [Get("/v1/breeds")]
        Task<IEnumerable<Breed>> GetBreeds();

        [Get("/v1/breeds")]
        Task<IEnumerable<BreedFilter>> GetBreedFilters();
    }
}
