﻿namespace mmc.FernandoPrestes.BFF.Services.RefitSample.Responses
{
    public class Weight
    {
        public string imperial { get; set; }
        public string metric { get; set; }
    }
}
