﻿namespace mmc.FernandoPrestes.BFF.Services.RefitSample.Responses
{
    public class BreedFilter
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Temperament { get; set; }
    }
}
