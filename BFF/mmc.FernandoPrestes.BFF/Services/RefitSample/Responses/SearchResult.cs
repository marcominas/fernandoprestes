﻿namespace mmc.FernandoPrestes.BFF.Services.RefitSample.Responses
{
    public class SearchResult
    {
        public string Id { get; set; }
        public string Url { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public Breed Breed { get; set; }
        public Breed[] Breeds { get; set; }
    }
}