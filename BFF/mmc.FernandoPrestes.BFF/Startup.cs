using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using mmc.FernandoPrestes.BFF.Config;
using mmc.FernandoPrestes.BFF.Core.Commands;
using mmc.FernandoPrestes.BFF.Core.Queries;
using mmc.FernandoPrestes.BFF.Core.Repositories;
using mmc.FernandoPrestes.BFF.Infra.Gateways;
using mmc.FernandoPrestes.BFF.Infra.Repositories;
using mmc.FernandoPrestes.BFF.Infra.ServiceGateways;
using System.Text.Json.Serialization;

namespace mmc.FernandoPrestes.BFF
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers()
                    .AddJsonOptions(options =>
                    {
                        options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                        options.JsonSerializerOptions.IgnoreNullValues = true;
                    });

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "mmc.FernandoPrestes.BFF API",
                    Description = "A simple example ASP.NET Core Web API"
                });
            });

            var refitConfigItems = Configuration.GetServicesConfig("Services");
            refitConfigItems.AddRefitClient<IUserServiceGateway>(services, "user-service");

            services.AddTransient(typeof(IUserGateway), typeof(UserGateway));
            services.AddTransient(typeof(IUserRepository), typeof(UserRepository));
            services.AddMediatR(
                                   typeof(CreateUserCommandHandler),
                                   typeof(GetUserByIdQueryHandler),
                                   typeof(ListUserQueryHandler)
                               );
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger(c =>
            {
                c.SerializeAsV2 = true;
            });

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "mmc.FernandoPrestes.BFF V1");
                // c.RoutePrefix = string.Empty;
            });


            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}