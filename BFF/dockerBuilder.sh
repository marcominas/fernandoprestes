#!/usr/bin/bash

# if [[ -z $(ps -ef | grep docker | grep -v grep) ]]; then
#     sudo service docker start
# fi

if [ "$1" == "build" ]; then
    echo "building bff"
    docker build --tag mmc-fernando-prestes-bff:1.0 .
fi

if [ "$1" == "run" ]; then
    echo "starting bff"
    docker rm --force bff > /dev/null 2>&1
    docker run -dp 5010:80 --name bff mmc-fernando-prestes-bff:1.0 
fi

if [ "$1" == "stop" ]; then
    echo "stopping bff"
    docker stop bff
fi
